const app = getApp();
Page({
  //  页面的初始数据
  data: {
      remarkText:"",
      qq:"",
      radio1: false,
      radio2: false,
      radio3: false
  },

  submitEvent() {
    let that = this;
    wx.showModal({
          title: '信息',
          content: '提交成功，管理员会及时将您的问题尽快解决！',
          showCancel: false,
          complete: function (sm) {
            that.onLoad();
          }
    });
  },

  bindTextAreaBlur(e) {
    // console.log(e);
  },

  onLoad: function() {
      this.setData({
          remarkText:"",
          qq:"",
          radio1: false,
          radio2: false,
          radio3: false
      });
  }
})
