//index.js
//获取应用实例
var util = require('../../utils/util.js')
const DB = wx.cloud.database().collection("bill");

var app = getApp()
Page({
  data: {
    todayExpend: "0",
    monthExpend: "0",
    yearExpend: "0",
    todayRecord:[],
    count: 1
  },
  //事件处理函数
  recodeExpend: function () {
    wx.navigateTo({
      url: '../../pages/record-expend/record-expend',
    })
  },
  showHistory: function() {
    wx.navigateTo({
      url: '../../pages/history-bill/history-bill',
    })
  },
//今日账单item点击
  onTodayBillItemClick:function(e){
    let index = e.currentTarget.dataset.index;

  },
    delData(id) {
    let that = this;
      DB.doc(id).remove({
        success(res) {
          console.log("删除成功");
          wx.setStorageSync("Bill","");
          that.onShow();
        },
        fail(err) {
          console.log("删除失败");
        }
      });
    },
//今日账单item长按
  ononTodayBillLongItemClick:function(e){
    // console.log(e.currentTarget.dataset.index);
    // let id = e.currentTarget.dataset.index;
    let id = e.currentTarget.dataset.index;

    let that = this
    wx.showModal({
      title: '提示',
      content: '您确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          // 用户点击了确定 可以调用删除方法了
          // console.log("id=",id);
          that.delData(id);
        } else if (sm.cancel) {
          console.log('用户点击取消')
        }
      }
    })

  },

  onLoad: function () {
    // let openid = wx.getStorageSync("openid");
    // if(openid === "") {
    //   wx.cloud.callFunction(
    //       {
    //         name: "getopenid",
    //         success(res) {
    //           console.log("openid获取成功",res.result.openid);
    //           try {
    //             wx.setStorageSync('openid', res.result.openid);
    //           } catch (e) {
    //           }
    //         },
    //         fail(err) {
    //           console.log("openid获取失败",err)
    //         }
    //       },
    //   )
    // }
  },
  getData(bill,todayDate) {
    let todayMoney = 0;
    let monthMoney = 0;
    let yearMoney = 0;
    let todayRecord = [];
    let openid = wx.getStorageSync("openid");
    for (let key of bill) {
      // console.log(key);

      if(openid !== key.openid) continue;
      // console.log(todayDate);
      //同一天
      if (util.dateIsDifference(key.date, todayDate, "d")) {
        todayMoney += key.spendMoney;
        todayRecord.push(key);
        // console.log(key,"===========================")
      };
      //同一月
      if (util.dateIsDifference(key.date, todayDate, "n")) {
        monthMoney += key.spendMoney;
      };
      //同一年
      if (util.dateIsDifference(key.date, todayDate, "y")) {
        yearMoney += key.spendMoney;
      };
    }
    this.setData({
      todayExpend: todayMoney,
      monthExpend: monthMoney,
      yearExpend: yearMoney,
      todayRecord:todayRecord,
    });
  },
  onShow: function () {
    let bill;
    const todayDate = util.formatTime(new Date(), "yyyy-MM-dd");
    try {
      bill = wx.getStorageSync('Bill');
      // console.log(bill);
    } catch (e) {
    }
    if (bill !== "") {
      this.getData(bill,todayDate);
    }else {
      // console.log("本地缓存为空");
      //1、去查询数据库
      let that = this;
      let openid;
      try {
        openid = wx.getStorageSync("openid");
      }catch (e) {

      }
      // console.log("openid",openid);
      wx.cloud.callFunction({
        name: "getList",
        data: {
          openid: openid
        },
        success(res) {
          console.log("请求云函数成功",res.result.data);

          //2、去遍历
          that.getData(res.result.data,that.todayDate);
          //3、写入本地缓存中
          if(res.result.data.length !== 0)
          wx.setStorageSync('Bill', res.result.data);

          // that.getData(bill,todayDate);

          // console.log(that.count)
          // if(that.count < 3) {
            that.onShow();
          //   that.setData({
          //     count: that.count + 1
          //   });
          // }
          // that.setData({
          //   bill: res.result.data
          // });
        },
        fail(err) {
          // console.log("请求云函数失败",err);
        }
      });
      // console.log("bill的值",bill);
    };
  },

  onShareAppMessage: function () {
    return {
      title: '账单',
      path: 'pages/index/index',
      success: function (res) {
        // 分享成功
      },
      fail: function (res) {
        // 分享失败
      }
    }
  },

})
