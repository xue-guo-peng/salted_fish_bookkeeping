// pages/history-bill/history-bill.js
var util = require('../../utils/util.js')

Page({
  data:{
    startdDate:"2020-01-01",
    endDate:util.formatTime(new Date(), "yyyy-MM-dd"),
    today: util.formatTime(new Date(), "yyyy-MM-dd"),
    historyBillList:[

    ],
    scrollHeight:"100%",
  },
  getData(bill,todayDate) {
    let todayRecord = [];
    let openid = wx.getStorageSync("openid");
    for (let key of bill) {
      if(openid !== key.openid) continue;
      //同一天
      // console.log(key);
      // console.log(todayDate);
      if (util.dateIsDifference(key.date, todayDate, "d")) {
        // todayMoney += key.spendMoney;
        todayRecord.push(key);
      };
    }
    this.setData({
      historyBillList:todayRecord,
    });
  },
  onLoad:function(options){
    // 页面初始化 options为页面跳转所带来的参数
    // let bill;
    // try {
    //   bill = wx.getStorageSync('Bill');
    // } catch (e) {
    //
    // }
    // if (bill !== "")
    //   this.getData(bill,util.formatTime(new Date(), "yyyy-MM-dd"));

    let that = this;
    this.setData({
      historyBillList: that.getDataByTime("2020-01-01",util.formatTime(new Date(), "yyyy-MM-dd"))
    });
  },
  onReady:function(){
    // 页面渲染完成
  },
  onShow:function(){
    // 页面显示
  },
  onHide:function(){
    // 页面隐藏
  },
  onUnload:function(){
    // 页面关闭
  },
  getDataByTime(start,end) {

    // console.log(start)
    // console.log(end)

    let bill = wx.getStorageSync("Bill");
    let num1 = util.formatToDate(start);
    let num2 = util.formatToDate(end);

    // console.log("num1",num1)
    // console.log("num2",num2)
    // console.log(bill);
    let openid = wx.getStorageSync("openid");

    let value = [];
    for(let key of bill) {
      if(key.openid !== openid) continue;
      let tag = util.formatToDate(key.date);
      // console.log("tag",tag)
      if(tag >= num1 && tag <= num2) {
        value.push(key);
      }
    }
    return value;
  },
  bindDateChangeStart(value) {
    // console.log("设置开始时间",value.detail);
    let data = this.getDataByTime(value.detail.value,util.formatTime(new Date(), "yyyy-MM-dd"));
    this.setData({
      startdDate:  value.detail.value,
      historyBillList: data
    });
  },
  bindDateChangeEnd(value) {
    // console.log("设置结束时间",value.detail);
    let that = this;
    this.setData({
      endDate:  value.detail.value,
      historyBillList: that.getDataByTime("2020-01-01",value.detail.value)
    });
  }
})
