var util = require('../../utils/util.js')

//index.js
//获取应用实例
var app = getApp()
Page({
    data: {
        optionColorId: 1,
        listAll: [{
            DateTime: "2020年度",
            Money: "10000元"
        }],
        listCat: [{
            C_Name: "淘宝",
            C_Money: "10000元"
        }]
    },
    //事件处理函数
    bindViewTap: function () {

    },
    onShow() {
        this.onLoad();
    },
    onLoad: function () {
        //1、取出数据
        //2、判断是否是同一年
        let bill;
        let openid;
        try {
            bill = wx.getStorageSync('Bill');
            openid = wx.getStorageSync("openid");
        } catch (e) {
        }

        //循环每一年的数据并且累加
        let todayDate = new Date().getFullYear();
        let data1 = [];

        let num1 = 0;//饮食
        let num2 = 0;//交通
        let num3 = 0;//淘宝
        let num4 = 0;//娱乐
        let num5 = 0;//房租
        let num6 = 0;//其他

        for(let i = 2020;i <= todayDate;i++) {
            let sum = 0;

            for (let key of bill) {
                if(openid !== key.openid) continue;
                //算钱
                if (util.dateIsDifference(key.date, i + "-12-31", "y")) {
                    sum += key.spendMoney;

                    switch (key.spendWay) {
                        case "饮食": num1 += key.spendMoney;break;
                        case "交通": num2 += key.spendMoney;break;
                        case "淘宝": num3 += key.spendMoney;break;
                        case "娱乐": num4 += key.spendMoney;break;
                        case "房租": num5 += key.spendMoney;break;
                        case "其他": num6 += key.spendMoney;break;
                    }
                };

/*                if(i > 2020) continue;
                switch (key.spendWay) {
                    case "饮食": num1 += key.spendMoney;break;
                    case "交通": num2 += key.spendMoney;break;
                    case "淘宝": num3 += key.spendMoney;break;
                    case "娱乐": num4 += key.spendMoney;break;
                    case "房租": num5 += key.spendMoney;break;
                    case "其他": num6 += key.spendMoney;break;
                }*/
            }
            // console.log(sum);
            let json1 = {
                DateTime: i + "年度",
                Money: sum + "元"
            };

            data1.push(json1);
        }

        let data2 = [
            {
                C_Name:"饮食",
                C_Money: num1 + "元"
            },
            {
                C_Name:"交通",
                C_Money: num2 + "元"
            },
            {
                C_Name:"淘宝",
                C_Money: num3 + "元"
            },
            {
                C_Name:"娱乐",
                C_Money: num4 + "元"
            },{
                C_Name:"房租",
                C_Money: num5 + "元"
            },{
                C_Name:"其他",
                C_Money: num6 + "元"
            }
        ]

        this.setData({
            listAll: data1,
            listCat: data2
        });
    },
    bindOptionTap(value) {

        let bill;
        let openid;
        try {
            bill = wx.getStorageSync('Bill');
            openid = wx.getStorageSync("openid");
        } catch (e) {
        }
        //循环每一年的数据并且累加
        let year = new Date().getFullYear();
        let Month = new Date().getMonth() + 1;
        let tempDate = new Date();
        tempDate.setFullYear(year);
        tempDate.setMonth(Month - 1);
        tempDate.setDate(1); // 本年初始时间
        let day = util.dateIndexInYear(new Date())
            - util.dateIndexInYear(tempDate) + 1;

        let data1 = [];
        let num1 = 0;//饮食
        let num2 = 0;//交通
        let num3 = 0;//淘宝
        let num4 = 0;//娱乐
        let num5 = 0;//房租
        let num6 = 0;//其他

        // console.log(value);
        if(value.target.id === '2') {
            //判断是不是同一个年，年消费
            for(let i = 1;i <= Month;i++) {
                let sum = 0;//记录今年每个月的消费

                for (let key of bill) {
                    if(openid !== key.openid) continue;
                    //算钱
                    if (util.dateIsDifference(key.date,  year + "-" + i + "-25", "n")) {
                        sum += key.spendMoney;

                        switch (key.spendWay) {
                            case "饮食": num1 += key.spendMoney;break;
                            case "交通": num2 += key.spendMoney;break;
                            case "淘宝": num3 += key.spendMoney;break;
                            case "娱乐": num4 += key.spendMoney;break;
                            case "房租": num5 += key.spendMoney;break;
                            case "其他": num6 += key.spendMoney;break;
                        }
                    };

/*                    if(i > 1) continue;
                    if (util.dateIsDifference(key.date,  year + "-" + i + "-25", "y")) {
                        switch (key.spendWay) {
                            case "饮食": num1 += key.spendMoney;break;
                            case "交通": num2 += key.spendMoney;break;
                            case "淘宝": num3 += key.spendMoney;break;
                            case "娱乐": num4 += key.spendMoney;break;
                            case "房租": num5 += key.spendMoney;break;
                            case "其他": num6 += key.spendMoney;break;
                        }
                    };*/
                }
                // console.log(sum);
                let json1 = {
                    DateTime: year + "年" + i + "月",
                    Money: sum + "元"
                };

                data1.push(json1);
            }

        }else if(value.target.id === '3') {
            //判断是不是同一个月，月明细
            for(let i = 1;i <= day;i++) {
                let sum = 0;
                for (let key of bill) {
                    if(openid !== key.openid) continue;
                    //算钱
                    if (util.dateIsDifference(key.date,  year + "-" + Month + "-" + i, "d")) {
                        sum += key.spendMoney;
                        switch (key.spendWay) {
                            case "饮食": num1 += key.spendMoney;break;
                            case "交通": num2 += key.spendMoney;break;
                            case "淘宝": num3 += key.spendMoney;break;
                            case "娱乐": num4 += key.spendMoney;break;
                            case "房租": num5 += key.spendMoney;break;
                            case "其他": num6 += key.spendMoney;break;
                        }

                    };
                }

                let json1 = {
                    DateTime: year + "年" + Month + "月" + i + "日",
                    Money: sum + "元"
                };

                data1.push(json1);
            }
        }else {
            //全部，重现加载即可
            this.onLoad();
            this.setData({
                optionColorId: value.target.id
            });
            return;
        }

        let data2 = [
            {
                C_Name:"饮食",
                C_Money: num1 + "元"
            },
            {
                C_Name:"交通",
                C_Money: num2 + "元"
            },
            {
                C_Name:"淘宝",
                C_Money: num3 + "元"
            },
            {
                C_Name:"娱乐",
                C_Money: num4 + "元"
            },{
                C_Name:"房租",
                C_Money: num5 + "元"
            },{
                C_Name:"其他",
                C_Money: num6 + "元"
            }
        ]

        this.setData({
            optionColorId: value.target.id,
            listAll:data1,
            listCat:data2
        });
    },
    onShareAppMessage: function () {
        return {
            title: '账单',
            path: 'pages/mine/mine',
            success: function (res) {
                // 分享成功
            },
            fail: function (res) {
                // 分享失败
            }
        }
    }
})
